# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="muse"
ZSH_THEME="ys"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

bindkey "^[[A" history-search-backward
bindkey "^[[B" history-search-forward

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(zsh-autosuggestions composer git symfony2 brew capistrano node osx npm docker tmux)

source $ZSH/oh-my-zsh.sh
source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#tmux
#if which tmux >/dev/null 2>&1; then
    ## if no session is started, start a new session
    #test -z ${TMUX} && tmux

    ## when quitting tmux, try to attach
    #while test -z ${TMUX}; do
    #    tmux attach || break
    #done
#fi

# Customize to your needs...
export ZSH_HIGHLIGHT_HIGHLIGHTERS_DIR=/usr/local/share/zsh-syntax-highlighting/highlighters
export NODE_PATH=/usr/local/share/npm/bin
export PATH=/Library/Developer/Toolchains/swift-latest.xctoolchain/usr/bin:./node_modules/.bin:/usr/local/bin:$PATH:/usr/bin:/bin:/usr/sbin:/sbin:$NODE_PATH:/usr/local/sbin
alias gg='git log --oneline --abbrev-commit --all --graph --decorate --color'
export HOMEBREW_GITHUB_API_TOKEN="b6d0a6046272d1c37fc4174f41b0c0b8a779186b"
source ~/.fresh/build/shell.sh

#export vmFile="~/Documents/Virtual\ Machine/dockerServer.vmwarevm/Ubuntu\ 15.04\ server64-bit.vmx"
export vmFile="~/Documents/Virtual\ Machine/docker-ubuntu_15.10.vmwarevm/docker-ubuntu_15.10.vmx"
alias vmstart="vmrun start $vmFile nogui"
alias vmstop="vmrun stop $vmFile nogui"
alias vmstatus="vmrun list"

#shortcuts commands
alias tmuxPluginFix="chmod +x ~/.tmux/plugins/tpm/scripts/* && chmod +x ~/.tmux/plugins/tpm/tpm && chmod +x ~/.tmux/plugins/tpm/bindings/install_plugins"

source ~/.iterm2_shell_integration.zsh

#export DOCKER_HOST=tcp://172.16.210.135:2375
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

