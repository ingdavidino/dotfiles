set nu
set expandtab
syntax enable
set term=screen-256color
set background=dark
set nocompatible
set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set showmatch
set ruler
set virtualedit=all

filetype off    " Required

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Plugin 'gmarik/vundle'    " Required
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-fugitive'

filetype plugin indent on " Required
