#!/bin/sh 
export PATH=$PATH:/usr/local/bin

# abort if we're already inside a TMUX session
[ "$TMUX" == "" ] || exit 0 

# startup a "default" session if none currently exists
#tmux has-session -t _default || tmux new-session -s _default -d

tmux new
#-s "$SESSION_NAME"
